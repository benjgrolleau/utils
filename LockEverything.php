<?php
/**
 * Class to lock all blocks.
 * Realised during working hours @Atelier Asap.
 * @see www.atelier-asap.com
 *
 * @author Benjamin Grolleau <hello@benjamin-grolleau.fr
 * @author Marie Comet <hello@mariecomet.fr>
 *
 */

declare( strict_types=1 );

namespace Benj\Utils;

class LockEverything {

	const LOCK_PARAMS = [ "move" => true, "remove" => true ];

	public function __construct() {
		$this->hooks();
	}

	public function hooks() {
		// No matters where you're hooking.
		add_action( 'admin_init', [ $this, 'lockEverything' ] );
	}

	public function lockEverything() {
		$posts = get_posts([
			'post_type' => 'page',
			'post_status' => 'publish',
			'posts_per_page' => -1,
		]);


		if( $posts ) :
			foreach( $posts as $post ) :
				setup_postdata( $post );
				if ( has_blocks( $post->ID ) ) :
					$blocks = parse_blocks( $post->post_content );
					if( $blocks ) :
						$new_blocks = serialize_blocks( $this->addLockingAttributes( $blocks ) );
						$updated_post = wp_update_post( [
							'ID' => $post->ID,
							'post_content' => $new_blocks,
						], false, false );
						echo get_permalink( $updated_post );
					endif;
				endif;
				wp_reset_postdata();
			endforeach;
		endif;
	}

	public function addLockingAttributes( array $blocks ) {
		$all_blocks = [];
		foreach( $blocks as $block ) :
			if( ! empty( $block['innerBlocks'] ) ) :
				$block['innerBlocks'] = $this->addLockingAttributes( $block['innerBlocks'] );
			endif;
			$block['attrs']['lock'] = self::LOCK_PARAMS;
			$all_blocks[] = $block;
		endforeach;
		return $all_blocks;
	}
}
